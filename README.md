# Exploit and Takeover a DJI Tello Drone
### Full Article
https://medium.com/@eyalasulin.9/exploit-and-takeover-a-dji-tello-drone-9f69c18f6a3a

### Example Video
https://youtu.be/sPcaYsSUuJE

## Contact 
Eyalasulin.9@gmail.com
https://www.linkedin.com/in/eyal-asulin-339901194/
